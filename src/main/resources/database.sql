--liquibase formatted sql
--changeset Akshay:insert-dummy-data
insert into department values(1, 'computer');

--changeset Akshay:insert-dummy-data-2
insert into department values(2, 'electronics');
insert into department values(3, 'mechanical');

insert into employee values(1, 'akshay', 1);
insert into employee values(2, 'rohit', 2);
insert into employee values(3, 'pathare', 3);
